#!/usr/bin/python
# coding=utf-8

#Programa que faz verificação no dia atual de um determinado termo em um determinado caderno do diário oficial do estado de São Paulo
#O resultado é enviado por email

#O programa deve ser usado da seguinte maneira:
#python verificacaodiaria.py [Servidor SMTP] [Login] [Senha] [Remetente] [Destinatario] [Caderno] [Termo]
#Os primeiros 5 campos são referentes ao envio do email
#[Caderno] é qual caderno do diário oficial será conferido
#[Termo] o termo buscado

#usando slate: https://github.com/timClicks/slate

import slate
import urllib
import time
import sys
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

def preenchezeros(numero):
    #trecho para converter a página no formato esperado do endereço do diário oficial
    i = 0
    pagina = str(numero)
    while (i < 4-len(str(numero))):
        pagina = '0'+pagina
        i+=1
    return pagina

#função para enviar o email notificando se encontrou ou não
def enviaemail(servidor, usuario,senha,de,para, assunto, conteudo):
    server = smtplib.SMTP(servidor,587)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(usuario,senha)

    msg = MIMEMultipart()
    msg['From'] = de
    msg['To'] = para
    msg['Subject'] = assunto

    msg.attach(MIMEText(conteudo,'plain','utf-8'))
    server.sendmail(de,para,msg.as_string())

#Salva em um arquivo o log de busca diário
def salvarlog(nomearquivo,conteudo,data):
    arquivo = open(nomearquivo, 'a')
    arquivo.write('\n' + data + '\n' + conteudo + '\n')
    arquivo.close


meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']

enderecobase = 'http://diariooficial.imprensaoficial.com.br/doflash/prototipo/'
caderno = sys.argv[6]
termobuscado = sys.argv[7]

#Pegando a data de hoje no formato que será utilizado no acesos ao diário oficial
ano = time.strftime("%Y")
dia = time.strftime("%d")
mes = meses[int(time.strftime("%m"))-1]


pagina = 0
result =-1
encontrou = 0
parar = False
textoEnvio = ''

filedownloaded = urllib.URLopener()

#inicialmente se procura o termo inicial 2 vezes (à partir da segunda ocorrencia, busca-se os termos desejados)
while not(parar):
    print str(pagina)
    pagina+=1
    try:
        filedownloaded.retrieve(enderecobase+ano+'/'+mes+'/'+dia+'/'+caderno+'/pdf/'+'pg_'+preenchezeros(pagina)+'.pdf', "arquivo.pdf")
    #caso tenham acabado as páginas e não encontrou o termo procurado
    #ou caso já na primeira página não encontrou (por não ter o caderno ou o dia)
    except Exception as inst:
        #O precisa tentar acessar novamente
        if inst[1] == 0 :
            pagina-=1
        else :
            parar = True

    with open ('arquivo.pdf') as arquivo:
        conteudo = slate.PDF(arquivo)

    result = conteudo[0].find(termobuscado)

    if (result > -1):
        encontrou +=1
        textoEnvio += '\n Encontrei na página ' + str(pagina)


if (encontrou == 0 and pagina==1):
    textoEnvio = 'Esse caderno não está disponível no diário oficial de hoje'
elif (encontrou == 0):
    textoEnvio = 'O termo "'+ termobuscado +'" não foi encontrado no diário oficial de hoje'

enviaemail(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5],'Busca Diário Oficial de '+ dia + '/' + mes + '/' + ano, textoEnvio)

salvarlog('resultadoBusca.txt',textoEnvio,dia + '/' + mes + '/' + ano)
