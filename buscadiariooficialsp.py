# coding=utf-8
import slate
import urllib

#usando slate: https://github.com/timClicks/slate
enderecobase = 'http://diariooficial.imprensaoficial.com.br/doflash/prototipo/'
ano = '2016'
mes = 'Março'
dia = '05'
caderno = 'exec1'
termoinicial = 'UNIDADE DE ATIVIDADES CULTURAIS'

#arquivo com os termos procurados, dentro desse arquivo deve ter uma palavra por linha
arqtermos = 'termos.txt'

def preenchezeros(numero):
    #trecho para converter a página no formato esperado do endereço do diário oficial
    i = 0
    pagina = str(numero)
    while (i < 4-len(str(numero))):
        pagina = '0'+pagina
        i+=1
    return pagina

pagina = 0
result =-1
encontrou = 0

filedownloaded = urllib.URLopener()

#inicialmente se procura o termo inicial 2 vezes (à partir da segunda ocorrencia, busca-se os termos desejados)
while encontrou < 2:
    pagina+=1
    print ('Pagina atual: '+str(pagina))
    try:
        filedownloaded.retrieve(enderecobase+ano+'/'+mes+'/'+dia+'/'+caderno+'/pdf/'+'pg_'+preenchezeros(pagina)+'.pdf', "arquivo.pdf")
    #caso tenham acabado as páginas e não encontrou o termo procurado
    except:
        break

    with open ('arquivo.pdf') as arquivo:
        conteudo = slate.PDF(arquivo)

    result = conteudo[0].find(termoinicial)

    if (result > -1):
        encontrou +=1

#Se encontrou à partir da segunda vez, inicia a busca por todos os termos desejados a partir da página onde encontrou o termoinicial a segunda vez
if encontrou == 2:
    #pegando todos os termos do arquivo indicado
    with open(arqtermos, 'r') as arquivo:
        termos = arquivo.read().splitlines()

    while True :

        #começar a partir da última página acessada
        with open ('arquivo.pdf') as arquivo:
            conteudo = slate.PDF(arquivo)

        for termoatual in termos:
            result = conteudo[0].find(termoatual)
            if (result > -1):
                print ('Encontrei o termo "' + termoatual + '" na página ' + str(pagina))

        #avança para a página seguinte
        pagina+=1
        print ('Pagina atual: '+str(pagina))
        try:
            filedownloaded.retrieve(enderecobase+ano+'/'+mes+'/'+dia+'/'+caderno+'/pdf/'+'pg_'+preenchezeros(pagina)+'.pdf', "arquivo.pdf")
        #caso tenham acabado as páginas e não encontrou o termo procurado
        except:
            print ('Acabaram as páginas deste diario oficial')
            break


else:
    print ('Não encontrei o termo inicial 2 vezes')
