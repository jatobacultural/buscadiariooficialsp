Busca Diario Oficial SP
-----------------------

Ferramenta para buscar determinadas palavras-chave no diário oficial do estado de São Paulo

* verificacaodiaria.py : Busca um determinado termo, em um determinado caderno no dia atual. Envia o resultado por email.

* buscadiariooficialsp.py: A ideia deste é buscar diversos termos no diário (em construção
